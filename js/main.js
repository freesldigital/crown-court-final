
$(function() {
    var $mainContent = $('#main');
    var $carousel = $('#slider');
    var $carouselImages = $carousel.find('.carousel-cell');
    var $carouselControls = $carousel.find('.slider-controls');
    var $carouselButtons = $carouselControls.find('a');
    
    //initCarousel($carousel);
    initSlider($carousel);
    
    function initCarousel (el) {
        el.flickity({
            // options
            cellAlign: 'center'
        });
    }
    
    function initSlider (el) {
        $carouselButtons.on('click', handleCarouselEvent);
        $carouselImages.swipe({
            swipe: handleCarouselSwipe,
            allowPageScroll: "vertical"
        });

        //$(window).on('resize', handleCarouselResize);
        
    }
    
    function handleCarouselSwipe(e, direction, distance, duration, fingerCount, fingerData) {
        if (direction === 'left' || direction === 'right') {
            handleCarouselEvent.bind(e.target.parentNode)(e);
        }   
    }
    
    function handleCarouselEvent (e) {
        if ($(this).hasClass('selected')) {
            return;
        }
        
        changeControls();
        changeImages();
    }
    function handleCarouselResize (e) {
        var heightMainContent = $mainContent.outerHeight();
        var bottomOffset = heightMainContent;
        $carouselControls.css('bottom', bottomOffset);
    }
    
    function changeControls () {
        $carouselButtons.toggleClass('selected');
    }
    
    function changeImages () {
        $carouselImages.toggleClass('selected');
    }
});